package aiguoclient

const (
	BASE_URL_PRODUCT = "http://api.aiguo.tech/"
	BASE_URL_DEV     = "http://testing-api.aiguo.tech/"
)

type Client struct {
	AppID     int
	AppKey    string
	AppSecret string
	BaseURL   string
}

func NewClient(appID int, appKey string, appSecret string, baseURL string) *Client {
	return &Client{
		AppID:     appID,
		AppKey:    appKey,
		AppSecret: appSecret,
		BaseURL:   baseURL,
	}
}
