package aiguoclient

import (
	"crypto/md5"
	"encoding/hex"
	"strings"
)

//内部函数
/*
	appID: 分配的渠道号
	appSecretKey: 分配的秘钥
*/
func (cli *Client) genSign(query string, body string) string {

	//去掉?
	sList := strings.Split(query, "?")
	newQ := sList[0]
	if len(sList) == 2 {
		newQ += sList[1]
	}

	rawStr := newQ + body + cli.AppSecret
	return calMd5(rawStr)
}

func calMd5(str string) string {
	h := md5.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}
