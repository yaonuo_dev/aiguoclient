package aiguoclient

import (
	"fmt"
	_ "image/gif"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

const APP_ID = 8979167826
const APP_KEY = "Pfah98HOujZPO3IEyn29"
const APP_SECRET = "Dj-ahcGNDJ93nWvHl4vNOD2nE9"

func New() *Client {
	return NewClient(APP_ID, APP_KEY, APP_SECRET, BASE_URL_DEV)
}

//测试下单
func TestAddOrder(t *testing.T) {

	//参数
	param := AddOrderOptions{
		RecoveryCategoryCode: CONF_CATEGORY_AIGO_CODE[CATEGORY_CLOTHES],
		CargoName:            "大衣",
		EstimatedWeight:      "1.5",
		Source:               "前端应用名",
		MeetBeginAt:          "2022-11-03 09:00:00",
		MeetEndAt:            "2022-11-03 10:00:00",
		RemarkFromBusiness:   "订单备注",
		SenderName:           "发货人姓名",
		SenderPhone:          "19821232111",
		SenderProvinceName:   "发货人所在省",
		SenderCityName:       "发货人所在市",
		SenderCountyName:     "发货人所在区",
		SenderStreetName:     "发货人所在的街道办事处（镇）",
		SenderDetail:         "发货人详细地址",
		ReceiverName:         "收货人姓名",
		ReceiverPhone:        "19821232009",
		ReceiverProvinceName: "收货人所在省",
		ReceiverCityName:     "收货人所在市",
		ReceiverCountyName:   "收货人所在区",
		ReceiverStreetName:   "收货人所在的街道办事处（镇）",
		ReceiverDetail:       "收货人详细地址",
	}
	innerOrderId := "12346"

	//请求
	httpStatusCode, result, err := New().AddOrder(innerOrderId, param)
	if err != nil || (httpStatusCode != 200 && httpStatusCode != 201) {
		t.Errorf("request failed, code[%d]msg[%v]", httpStatusCode, result)
	}
}

//测试获取轨迹
func TestListTrace(t *testing.T) {

	//参数
	/*
		expressCompanyCode 快递公司编号
		trackingNumber     要查询的运单号
	*/
	expressCompanyCode := "SF"
	trackingNumber := "123456"

	//运单轨迹
	httpStatusCode, result, err := New().ListTrace(expressCompanyCode, trackingNumber)
	if err != nil || (httpStatusCode != 200 && httpStatusCode != 201) {
		t.Errorf("request failed, code[%d]msg[%v]", httpStatusCode, result)
	}
}

//测试取消
func TestCancelOrder(t *testing.T) {

	//参数
	param := CancelOrderOptions{
		CancelledReasonText: "地址写错了",
	}
	innerOrderId := "12345"

	//请求
	/*
		201取消成功
		200已是取消状态
		409当前状态不支持取消
		404订单不存在
	*/
	httpStatusCode, result, err := New().CancelOrder(innerOrderId, param)
	if err != nil || (httpStatusCode != 200 && httpStatusCode != 201) {
		t.Errorf("request failed, code[%d]msg[%v]", httpStatusCode, result)
	}
}

//aiguo 给 顺丰 下单成功
func TestExpressOrderSentCallback(t *testing.T) {

	w := httptest.NewRecorder()
	payload := strings.NewReader("{\"business_notification_id\":14,\"notification_type\":\"ExpressOrderSent\",\"business_notification_created_at\":\"2019-07-01 09:45:57\",\"data\":{\"out_business_order_number\":\"20190701094522762524\",\"in_business_order_number\":\"19070109453754540216\",\"express_order_number\":\"POTO1567355600489690\",\"express_recovery_number\":\"123456\",\"express_order_successfully_sent_at\":null,\"express_company_name\":\"\",\"express_company_code\":\"\"}}")
	req := httptest.NewRequest(http.MethodPut, "http://127.0.0.1:8128/notify", payload)

	//调用
	notifyType, resp, err := AiGuoCallback(w, req)
	if err != nil || notifyType != NOTIFY_CATEGORY_SENT {
		t.Errorf("request failed:%s", err.Error())
		return
	}
	fmt.Printf("%s", resp.(ExpressOrderSentCallbackResponse).Data.ExpressOrderNumber)
}

//分配快递员成功，开始去取件
//-->此时可以查出【快递员的name和手机号】
func TestExpressOrderReceiptingCallback(t *testing.T) {

	w := httptest.NewRecorder()
	payload := strings.NewReader("{\"business_notification_id\":21,\"notification_type\":\"ExpressOrderReceipting\",\"business_notification_created_at\":\"2019-06-28 15:30:09\",\"data\":{\"out_business_order_number\":\"20190628072908701\",\"in_business_order_number\":\"19062803290828079501\",\"express_order_number\":\"POTO1543495873755519\",\"express_recovery_number\":\"123456\",\"couriers\":{\"courier_real_name\":null,\"courier_job_number\":null,\"courier_phone\":null},\"receipting_at\":\"2019-07-01 15:53:01\"}}")
	req := httptest.NewRequest(http.MethodPut, "http://127.0.0.1:8128/notify", payload)

	//调用
	notifyType, resp, err := AiGuoCallback(w, req)
	if err != nil || notifyType != NOTIFY_CATEGORY_RECEIPTING {
		t.Errorf("request failed:%s", err.Error())
		return
	}
	fmt.Printf("%s", resp.(ExpressOrderReceiptingCallbackResponse).Data.Couriers.CourierPhone)
}

//取件成功
//此时可以拿到【真实重量 和 数量】
func TestExpressOrderGotCallback(t *testing.T) {

	w := httptest.NewRecorder()
	payload := strings.NewReader("{\"business_notification_id\":18,\"notification_type\":\"ExpressOrderGot\",\"business_notification_created_at\":\"2019-06-28 15:28:43\",\"data\":{\"out_business_order_number\":\"20190628072324721\",\"in_business_order_number\":\"19062803232469197963\",\"express_order_number\":\"POTO1543477072416136\",\"express_recovery_number\":null,\"express_company_name\":\"德邦\",\"express_company_code\":\"DEPPON\",\"weight_from_express\":\"15.00\",\"number_from_express\":1,\"freight_from_express\":616,\"express_order_waybills\":[{\"surface_number\":\"\",\"tracking_number\":\"5163080174\"}],\"warehouse_code\":\"801\",\"express_got_at\":\"2019-06-28 15:28:43\"}}")
	req := httptest.NewRequest(http.MethodPut, "http://127.0.0.1:8128/notify", payload)

	//调用
	notifyType, resp, err := AiGuoCallback(w, req)
	if err != nil || notifyType != NOTIFY_CATEGORY_GOT {
		t.Errorf("request failed:%s", err.Error())
		return
	}
	fmt.Printf("%s", resp.(ExpressOrderGotCallbackResponse).Data.WeightFromExpress)
}

//-->收件人签收成功
func TestExpressOrderSignedFromExpressCallback(t *testing.T) {

	w := httptest.NewRecorder()
	payload := strings.NewReader("{\"business_notification_id\":8,\"notification_type\":\"ExpressOrderSignedFromExpress\",\"business_notification_created_at\":\"2019-06-28 15:09:59\",\"data\":{\"out_business_order_number\":\"20190628065939610\",\"in_business_order_number\":\"19062802593947430819\",\"express_order_number\":\"POTO1543358241488164\",\"express_recovery_number\":null,\"express_company_name\":\"德邦\",\"express_company_code\":\"DEPPON\",\"weight_from_express\":\"15.00\",\"number_from_express\":1,\"freight_from_express\":616,\"express_order_waybills\":[{\"tracking_number\":\"5163080174\",\"surface_number\":\"\"}],\"express_signed_at\":\"2019-06-28 15:09:59\"}}")
	req := httptest.NewRequest(http.MethodPut, "http://127.0.0.1:8128/notify", payload)

	//调用
	notifyType, resp, err := AiGuoCallback(w, req)
	if err != nil || notifyType != NOTIFY_CATEGORY_SIGNEDFROMEXPRESS {
		t.Errorf("request failed:%s", err.Error())
		return
	}
	fmt.Printf("%s", resp.(ExpressOrderSignedFromExpressCallbackResponse).Data.ExpressSignedAt)
}

//-->取消物流
func TestExpressOrderCancelledCallback(t *testing.T) {

	w := httptest.NewRecorder()
	payload := strings.NewReader("{\"business_notification_id\":4,\"notification_type\":\"ExpressOrderCancelled\",\"business_notification_created_at\":\"2019-06-28 14:25:30\",\"data\":{\"out_business_order_number\":\"20190628061247178\",\"in_business_order_number\":\"19062802124961770023\",\"express_order_number\":\"POTO1543037913354640\",\"express_recovery_number\":\"\",\"cancelled_at\":\"2019-06-28 14:25:30\",\"cancelled_reason_text\":\"用户撤销订单\",\"cancelled_reason_code\":\"OTHER\",\"cancelled_before_order_status\":\"SENT\",\"express_order_waybills\":{\"tracking_number\":\"123456\"}}}")
	req := httptest.NewRequest(http.MethodPut, "http://127.0.0.1:8128/notify", payload)

	//调用
	notifyType, resp, err := AiGuoCallback(w, req)
	if err != nil || notifyType != NOTIFY_CATEGORY_CANCELLED {
		t.Errorf("request failed:%s", err.Error())
		return
	}
	fmt.Printf("%s", resp.(ExpressOrderCancelledCallbackResponse).Data.CancelledReasonText)
}

//-->取消aiguo订单
func TestAiGuoOrderCancelledCallback(t *testing.T) {

	w := httptest.NewRecorder()
	payload := strings.NewReader("{\"business_notification_id\":46,\"notification_type\":\"BusinessOrderCancelled\",\"business_notification_created_at\":\"2019-07-01 14:07:24\",\"data\":{\"out_business_order_number\":\"20190701140722760441\",\"in_business_order_number\":\"19070102072267288078\",\"business_order_cancelled_at\":\"2019-07-01 14:07:24\",\"cancelled_reason_code\":\"\",\"cancelled_reason_text\":\"\"}}")
	req := httptest.NewRequest(http.MethodPut, "http://127.0.0.1:8128/notify", payload)

	//调用
	notifyType, resp, err := AiGuoCallback(w, req)
	if err != nil || notifyType != NOTIFY_CATEGORY_AIGUO_CANCEL {
		t.Errorf("request failed:%s", err.Error())
		return
	}
	fmt.Printf("%s", resp.(AiGuoOrderCancelledCallbackResponse).Data.CancelledReasonText)
}

//测试查询订单详情
func TestQueryOrder(t *testing.T) {

	orderNo := "202201111009103439319344"

	//运单轨迹
	httpStatusCode, result, err := New().QueryOrder(orderNo)
	if err != nil || (httpStatusCode != 200 && httpStatusCode != 201) {
		t.Errorf("request failed, code[%d]msg[%v]", httpStatusCode, result)
	}
}
