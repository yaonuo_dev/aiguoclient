package aiguoclient

const (
	CATEGORY_CLOTHES = 1 //	衣物
	CATEGORY_MOBILE  = 2 // 移动电话
	CATEGORY_BOOK    = 3 //	书籍
	CATEGORY_OTHER   = 4 //	其它
)

//所有的默认类型配置
var CONF_CATEGORY_AIGO_CODE = map[int]string{
	CATEGORY_CLOTHES: "CLOTHES",
	CATEGORY_MOBILE:  "MOBILE",
	CATEGORY_BOOK:    "BOOK",
	CATEGORY_OTHER:   "OTHER",
}

const (
	NOTIFY_CATEGORY_SENT              = 1 //	aiguo 给 顺丰 下单成功
	NOTIFY_CATEGORY_RECEIPTING        = 2 //    分配快递员成功，开始去取件
	NOTIFY_CATEGORY_GOT               = 3 //	取件成功
	NOTIFY_CATEGORY_SIGNEDFROMEXPRESS = 4 //	收件人签收成功
	NOTIFY_CATEGORY_CANCELLED         = 5 //	取消物流
	NOTIFY_CATEGORY_AIGUO_CANCEL      = 6 //    aighuo取消
)

//通知类型
var CONF_NOTIFY_CATEGORY = map[int]string{
	NOTIFY_CATEGORY_SENT:              "ExpressOrderSent",
	NOTIFY_CATEGORY_RECEIPTING:        "ExpressOrderReceipting",
	NOTIFY_CATEGORY_GOT:               "ExpressOrderGot",
	NOTIFY_CATEGORY_SIGNEDFROMEXPRESS: "ExpressOrderSignedFromExpress",
	NOTIFY_CATEGORY_CANCELLED:         "ExpressOrderCancelled",
	NOTIFY_CATEGORY_AIGUO_CANCEL:      "BusinessOrderCancelled",
}
