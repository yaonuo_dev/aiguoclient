module gitee.com/yaonuo_dev/aiguoclient

go 1.13

require (
	github.com/astaxie/beego v1.12.1
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/shopspring/decimal v1.3.1
	github.com/w3liu/go-common v0.0.0-20210108072342-826b2f3582be
	github.com/xxjwxc/public v0.0.0-20200526160023-d8d1bd6babeb
	golang.org/x/image v0.0.0-20200430140353-33d19683fad8
)
